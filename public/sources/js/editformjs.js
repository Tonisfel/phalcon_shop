function createInput(countForName){
    let inputHidden = document.createElement("input");
    let input = document.createElement("input");
    let div = document.createElement("div");
    let div_btn_group = document.createElement("div");
    let btn = document.createElement("button");

    btn.name = "product-remove-btn";
    btn.type = "button";
    $(btn).addClass("btn btn-default");
    $(btn).text("Remove");
    $(btn).on("click", function() {
        $("#product-pictures-count").val(Number($("#product-pictures-count").val()) - 1);
        $(this).parent().parent().remove();
    });
    div.id = "product-url-" + countForName;
    $(div).addClass("input-group");
    $(div_btn_group).addClass("input-group-btn");
    input.type = "text";
    input.name = "product-url-" + countForName;
    $(input).addClass("form-control");
    inputHidden.type = "hidden";
    inputHidden.name = "product-id-" + countForName;
    div.append(inputHidden);
    div.append(input);
    div_btn_group.append(btn);
    div.append(div_btn_group);
    return div;
}

function removeOnClick(element){
    $("#product-pictures-count").val(Number($("#product-pictures-count").val()) - 1);
    $(element).parent().parent().remove();
    $.ajax({
       url : "/index/deletePic/" + $(element).parent().parent().children(":first").val()
    });
}

$("#product-pic-count-increase-btn").on("click", function() {
   $("#product-pictures-count").val(Number($("#product-pictures-count").val()) + 1);
   $("#product-pic-count-decrease-btn").attr("disabled", false);
   $("#product-count-urls-div").append(createInput($("#product-pictures-count").val()));
});

if ($("#product-pictures-count").val() == 0){
    $("#product-pic-count-decrease-btn").attr("disabled", true);
}
