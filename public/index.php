<?php

use Phalcon\Di;
use Phalcon\Loader;
use Phalcon\Mvc\Application;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Router;
use Phalcon\Http\Request;
use Phalcon\Http\Response;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\ViewBaseInterface;
use Phalcon\Mvc\View\Engine\Volt;
use Phalcon\Db\Adapter\Pdo\Mysql;
use Phalcon\Storage\SerializerFactory;
use Phalcon\Dispatcher;
use Phalcon\Events\Event;
use Phalcon\Cache\Adapter\Stream;
use Phalcon\Mvc\Dispatcher as MvcDispatcher;
use Phalcon\Mvc\Model\Manager as ModelManager;
use Phalcon\Mvc\Model\Metadata\Memory as ModelMetadata;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Mvc\Dispatcher\Exception as DispatchException;

/**
 * Very simple MVC structure
 */

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/apps');

$loader = new Loader();

$loader->registerDirs(
    [
        "../apps/controllers/",
        "../apps/models/",
    ]
);

$loader->register();

$di = new FactoryDefault();

// Registering a router
$di->set("router", function() {
    $router = new Router();
    $router->add("/", [
       "controller" => "index",
       "action" => "index"
    ]);
    $router->add("/index/add", ["controller" => "index",
        "action" => "add"]);

    $router->add("/index/remove/{id:[0-9]+}",
        [
            "controller" => "index",
            "action" => "remove",
        ]);

    $router->add("/index/{response:[a-z]+}",
        [
            "controller" => "index",
            "action" => "index"
        ]
    );

    $router->add("/index/add/{response:[a-z]+}",
        [
            "controller" => "index",
            "action" => "add"
        ]
    );

    $router->add("/index/edit/{id:[0-9]+}",
        [
            "controller" => "index",
            "action" => "edit"
        ]
    );

    $router->add("/index/deletePic/{id:[0-9]+}",
        [
            "controller" => "index",
            "action" => "deletePic",
        ]
    );

    $router->add("/index/edit/{id:[0-9]+}/{response:[a-z]+}",
        [
            "controller" => "index",
            "action" => "edit"
        ]
    );

    $router->add("/index/create", ["controller" => "index", "action" => "create"]);
    $router->add("/index/editproduct", ["controller" => "index", "action" => "editproduct"]);

    $router->notFound([
       "controller" => "index",
       "action" => "notFound",
    ]);
    return $router;
});

// Registering a dispatcher
$di->set(
    'dispatcher',
    function () {
        // Create an EventsManager
        $eventsManager = new EventsManager();

        // Attach a listener
        $eventsManager->attach(
            'dispatch:beforeException',
            function (Event $event, $dispatcher, Exception $exception) {
                // Handle 404 exceptions
                if ($exception instanceof DispatchException) {
                    $dispatcher->forward(
                        [
                            'controller' => 'index',
                            'action'     => 'notFound',
                        ]
                    );

                    return false;
                }

                // Alternative way, controller or action doesn't exist
                switch ($exception->getCode()) {
                    case 2:
                    case 5:
                        $dispatcher->forward(
                            [
                                'controller' => 'index',
                                'action'     => 'notFound',
                            ]
                        );

                        return false;
                }
            }
        );

        $dispatcher = new MvcDispatcher();

        // Bind the EventsManager to the dispatcher
        $dispatcher->setEventsManager($eventsManager);

        return $dispatcher;
    }
);

// Registering a Http\Response
$di->set("response", Response::class);

// Registering a Http\Request
$di->set("request", Request::class);

$di->set(
    "voltService",
    function (ViewBaseInterface $view) use ($di) {
        if (!file_exists(APP_PATH . "/storage/cache/volt/")) {
            mkdir(APP_PATH . "/storage/cache/volt/");
            chmod(APP_PATH . "/storage/cache/volt/", 0777);
        }
        $volt = new Volt($view, $di);
        $volt->setOptions(
            [
                'always'    => true,
                'extension' => '.php',
                'separator' => '_',
                'stat'      => true,
                'path'      => APP_PATH . '/storage/cache/volt/',
                'prefix'    => 'pr',
            ]
        );

        return $volt;
    }
);



// Registering the view component
$di->set(
    "view",
    function () {
        $view = new View();

        $view->setViewsDir("../apps/views/");

        $view->registerEngines(
              [
                  ".volt" => "voltService",
              ]
        );

        return $view;
    }
);

$di->set("db", function() {
    /**
     * @var $config array
     */
    include "../config/config.php";
    $connection = new Mysql($config);
    return $connection;
});

//Registering the Models-Metadata
$di->set("modelsMetadata", ModelMetadata::class);

//Registering the Models Manager
$di->set("modelsManager", ModelManager::class);

$di->set("cache", function() {
    $serializerFactory = new SerializerFactory();

    $options = [
        'defaultSerializer' => 'Json',
        'lifetime'          => 7200,
        'storageDir'        => APP_PATH . '/storage/cache/data/',
    ];

    $adapter = new Stream($serializerFactory, $options);
    return $adapter;
}, true);

try {
    $application = new Application($di);

    $response = $application->handle($_SERVER["REQUEST_URI"]);

    $response->send();
} catch (Exception $e) {
    print_r($e->getTraceAsString());
    echo $e->getMessage();
}
