<?php

use Phalcon\Mvc\Model;

class Products extends Model
{
    protected $id;
    protected $name;
    protected $price;

    public function initialize()
    {

    }

    public function reset(){
        parent::reset();
    }

    public function setName($name){
        if (strlen($name) < 4)
            throw new InvalidArgumentException("Too short name.");
        $this->name = $name;
    }

    public function getName(){
        return $this->name;
    }

    public function getId(){
        return $this->id;
    }

    public function getPrice(){
        return $this->price;
    }

    public function setPrice($price){
        if (!is_double($price) && !is_numeric($price)){
            throw new InvalidArgumentException("Not numeric price.");
        }
        $this->price = $price;
    }
}
