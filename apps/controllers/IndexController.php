<?php

use Phalcon\Mvc\Controller;

class IndexController extends Controller
{
    public function indexAction()
    {
        $this->cache->set("jopa", $this->db->fetchAll("SELECT
                                                                     `products`.`id`,
                                                                     `products`.`name`,
                                                                     `products`.`price`, 
                                                                     count(`pictures`.`id`) as pic_count
                                                                 FROM `products`
                                                                 LEFT JOIN `pictures` 
                                                                 ON `products`.`id` = `pictures`.`product_id` 
                                                                 GROUP BY `products`.`id` "), 3600);

        try {
            $headerCss = $this->assets->collection("headerCss");
            $headerJs = $this->assets->collection("headerJs");

            $headerCss->addCss("//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css", false);
            $headerCss->addCss("//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css", false);
            $headerCss->addCss("sources/css/style-ap.css", true);
            $headerJs->addJs("https://code.jquery.com/jquery-3.5.1.min.js", false);
            $headerJs->addJs("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js", false);

            //print_r($this->sources);
            //exit;
        } catch (Exception $ex){
            echo $ex->getMessage();
        }

        $dbResult = $this->cache->get("jopa", []);
        $this->view->products = $dbResult;

        $this->view->productsCount = count($dbResult);
        //Алерты
        $response = $this->dispatcher->getParam("response");

        if ($response == "deletesuccess"){
            $this->view->responseValue = "deletesuccess";
        }
        elseif ($response == "deletefailed"){
            $this->view->responseValue = "deletefailed";
        }
        elseif ($response == "createsuccess"){
            $this->view->responseValue = "createsuccess";
        }
        elseif ($response == "editsuccess"){
            $this->view->responseValue = "editsuccess";
        }
        elseif ($response == "invalidid"){
            $this->view->responseValue = "invalidid";
        } else {
            $this->view->responseValue = "response";
        }
    }

    public function addAction(){
        $headerCss = $this->assets->collection("headerCss");
        $headerJs = $this->assets->collection("headerJs");

        $headerCss->addCss("//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css", false);
        $headerCss->addCss("//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css", false);
        $headerCss->addCss("sources/css/style-ap.css", true);
        $headerJs->addJs("https://code.jquery.com/jquery-3.5.1.min.js", false);
        $headerJs->addJs("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js", false);

        $response = $this->dispatcher->getParam("response");
        if ($response == "invalid") {
            $this->view->responseValue = "invalid";
        } else {
            $this->view->responseValue = "response";
        }
    }

    public function createAction(){
        /** TODO: Редирект поправить (юзать алерты).
         *
         */
        $form = new \Phalcon\Forms\Form();

        $form->add(new \Phalcon\Forms\Element\Text('product-name'));
        $form->add(new \Phalcon\Forms\Element\Numeric('product-price'));

        $product = new Products();

        $form->bind($_POST, $product);

        if ($form->isValid() === true) {
            try {
                $product->setName($form->getValue('product-name'));
                $product->setPrice($form->getValue('product-price'));
            } catch (Exception $ex) {
                return $this->response->redirect("index/add/invalid");
            }
            if ($product->save() === true) {
                return $this->response->redirect("/index/createsuccess");
            }
        }
    }

    public function removeAction(){
        /**TODO: редирект через $this->response->redirect;
        Получение параметров POST, GET, REQUEST через $this->dispatcher.
        Избавиться от инъекций.
        Алерты.**/
        echo $this->dispatcher->getParam("id");
        $sql = "DELETE FROM `products` WHERE id = " . $this->dispatcher->getParam("id", "int");
        $result = $this->db->execute($sql);
        if ($result > 0) {
            return $this->response->redirect("/index/deletesuccess");
        } else {
            return $this->response->redirect("/index/deletefailed");
        }
    }

    public function editAction(){
        /** TODO: проделать защиту от дебилов.
         * Именованный роутинг. (в роутере сделать юрлы)
         * Посмотреть получение параметров.
         * Получение картинки из бд (только юрл, юрл любой).
         * При редактировании товара добавлять n-кол-во картинок и вывод их кол-ва в таблицу.
         * При редактировании отображать все картинки и сделать их добавление\удаление.
         * Убрать инъекции.
         */
        $headerCss = $this->assets->collection("headerCss");
        $headerJs = $this->assets->collection("headerJs");
        $footerJs = $this->assets->collection("footerJs");

        $headerCss->addCss("//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css", false);
        $headerCss->addCss("//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css", false);
        $headerCss->addCss("sources/css/style-ap.css", true);
        $headerJs->addJs("https://code.jquery.com/jquery-3.5.1.min.js", false);
        $headerJs->addJs("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js", false);
        $footerJs->addJs("sources/js/editformjs.js", true);

        try {
            $result = $this->db->fetchOne("SELECT 
                                                           `name`, 
                                                           `price`,
                                                           (
                                                                SELECT count(*)
                                                                FROM `pictures`
                                                                WHERE `product_id` = " . $this->dispatcher->getParam("id", "int") . "
                                                           ) AS `pic_count`
                                                     FROM `products` 
                                                     WHERE `id` = " . $this->dispatcher->getParam("id", "int"));
            $picsUrls = $this->db->fetchAll("SELECT `id`,`url` FROM `pictures` WHERE `product_id` = " . $this->dispatcher->getParam("id", "int") );
        } catch (Exception $e){
            return $this->response->redirect("/index/invalidid");
        }

        $this->view->product_id = $this->dispatcher->getParam("id", "int");
        $this->view->product_name = $result["name"];
        $this->view->product_price = $result["price"];
        $this->view->product_pic_count = $result["pic_count"];
        $this->view->urls = $picsUrls;

        $response = $this->dispatcher->getParam("response", "string");
        if ($response == "editpicinvalid"){
            $this->view->responseValue = "editpicinvalid";
        }
        elseif ($response == "editfailed") {
            $this->view->responseValue = "editfailed";
        }
        elseif ($response == "editinvalid") {
            $this->view->responseValue = "editinvalid";
        }
        elseif ($response == "invalidprice") {
            $this->view->responseValue = "invalidprice";
        }
        elseif ($response == "invalidname"){
            $this->view->responseValue = "invalidname";
        }
        else {
            $this->view->responseValue = "response";
        }
    }

    public function editproductAction(){
        $form = new \Phalcon\Forms\Form();
        $form->add(new \Phalcon\Forms\Element\Numeric("product-id"));
        $form->add(new \Phalcon\Forms\Element\Text("product-name"));
        $form->add(new \Phalcon\Forms\Element\Numeric("product-price"));
        $form->add(new \Phalcon\Forms\Element\Numeric("product-pictures-count"));
        $id = $form->getValue("product-id");
        $name = $form->getValue("product-name");
        $price = $form->getValue("product-price");
        $picsCount = $form->getValue("product-pictures-count");
        //Если картинки есть
        for ($i = 1; $i <= $picsCount; $i++){
            $form->add(new \Phalcon\Forms\Element\Numeric("product-id-$i"));
            $form->add(new \Phalcon\Forms\Element\Text("product-url-$i"));
            if ($form->getValue("product-url-$i") === ""){
                return $this->response->redirect("index/edit/$id/editpicinvalid");
            }
            if ($form->getValue("product-id-$i") === ""){
                $query = $this->buildSQLInsertPics(null, $id, $form->getValue("product-url-$i"));
            } else {
                $query = $this->buildSQLInsertPics($form->getValue("product-id-$i"), $id, $form->getValue("product-url-$i"));
            }
            if ($form->isValid() === true){
                if ($picsCount > 0) {
                    try {
                        $this->db->execute($query);
                    } catch (Exception $ex) {
                        return $this->response->redirect("index/edit/$id/editfailed");
                    }
                }
                if (!is_numeric($price) && !is_double($price)){
                    return $this->response->redirect("index/edit/$id/invalidprice");
                }
                if (strlen($name) < 4){
                    return $this->response->redirect("index/edit/$id/invalidname");
                }
            } else {
                return $this->response->redirect("index/edit/$id/editinvalid");
            }
        }
        //Если картинок нет
        if ($picsCount == 0){
            $this->db->execute("DELETE FROM `pictures` WHERE `product_id` = $id");
        }
        if ($form->isValid() === true){
            $this->db->execute("UPDATE `products` SET `name` = '$name', `price` = $price WHERE `id` = $id");
            return $this->response->redirect("/index/editsuccess");
        } else {
            return $this->response->redirect("index/edit/$id/editinvalid");
        }
    }

    public function notFoundAction(){
        $headerCss = $this->assets->collection("headerCss");
        $headerJs = $this->assets->collection("headerJs");

        $headerCss->addCss("//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css", false);
        $headerCss->addCss("//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css", false);
        $headerCss->addCss("sources/css/style-ap.css", true);
        $headerJs->addJs("https://code.jquery.com/jquery-3.5.1.min.js", false);
        $headerJs->addJs("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js", false);
    }

    public function deletePicAction(){
        $id = $this->dispatcher->getParam("id");
        $query = "DELETE FROM `pictures` WHERE `id`=$id";
        $this->db->execute($query);
    }

    private function buildSQLInsertPics($pic_id = null, $id = null, $url = ""){
        if ($pic_id === null){
            $sqlQuery = "INSERT INTO `pictures` (`id`, `product_id`, `url`) 
                      VALUES (NULL, $id, '$url')
                      ON DUPLICATE KEY UPDATE `url` = '$url'";
        } else {
            $sqlQuery = "INSERT INTO `pictures` (`id`, `product_id`, `url`) 
                      VALUES ($pic_id, $id, '$url')
                      ON DUPLICATE KEY UPDATE `url` = '$url'";
        }
        return $sqlQuery;
    }
}
