<html>
    <head>
        <?= $this->assets->outputCss('headerCss') ?>
        <?= $this->assets->outputJs('headerJs') ?>
    </head>
    <body>
        <nav class="navbar">
            <a href="/">Home</a>
            <a href="/index/add/">Add</a>
        </nav>
        <h1>Welcome</h1><?php if ($responseValue === 'createsuccess') { ?><div class="alert alert-success">Product has been created successfully.</div><?php } elseif ($responseValue === 'deletesuccess') { ?><div class="alert alert-success">Product has been deleted successfully.</div><?php } elseif ($responseValue === 'deletefailed') { ?><div class="alert alert-danger">Product deleting is failed. Contact to administrator.</div><?php } elseif ($responseValue === 'invalidid') { ?><div class="alert alert-danger">Product with that ID does not exist.</div><?php } elseif ($responseValue === 'editsuccess') { ?><div class="alert alert-success">Product has been edited successfully.</div><?php } ?><table class="table">
            <thead>
            <tr>
                <td>Name</td>
                <td>Price</td>
                <td>Count pictures</td>
                <td></td>
                <td></td>
            </tr>
            </thead>
            <tbody><?php if ($productsCount === 0) { ?><tr>
                <td colspan="5" style="text-align: center;"><span class="glyphicon glyphicon-info-sign"></span> No registered products.</td>
            </tr><?php } else { ?><?php foreach ($products as $product) { ?>
                <tr>
                    <td><?= $product->name ?></td>
                    <td><?= $product->price ?></td>
                    <td><?= $product->pic_count ?></td>
                    <td><a class="btn btn-default" href="/index/remove/<?= $product->id ?>">Remove</a></td>
                    <td><a class="btn btn-default" href="/index/edit/<?= $product->id ?>">Edit</a></td>
                </tr>
            <?php } ?><?php } ?></tbody>
        </table>
    </body>
</html>