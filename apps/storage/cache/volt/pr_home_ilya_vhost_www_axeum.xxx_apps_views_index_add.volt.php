<html>
    <head>
        <?= $this->assets->outputCss('headerCss') ?>
        <?= $this->assets->outputJs('headerJs') ?>
    </head>
    <body>
        <nav class="navbar">
            <a href="/">Home</a>
            <a href="/index/add/">Add</a>
        </nav>
        <h1>Add new product</h1><?php if ($responseValue === 'invalid') { ?><div class="alert alert-danger">Invalid name or price. Name must be longer then 4 and price must not be empty.</div><?php } ?><form action="/index/create" method="post">
            <label for="product-name">Product name:</label>
            <input class="form-control" id="product-name" name="product-name" type="text" placeholder="Type a name of new product">
            <br>
            <label for="product-price">Product price:</label>
            <input class="form-control" id="product-price" name="product-price" type="text" placeholder="Type a price of new product">
            <hr>
            <button class="btn btn-default" type="submit" name="product-create">Create</button>
        </form>
    </body>
</html>