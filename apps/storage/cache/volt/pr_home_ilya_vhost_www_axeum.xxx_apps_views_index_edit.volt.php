<html>
    <head>
        <?= $this->assets->outputCss('headerCss') ?>
        <?= $this->assets->outputJs('headerJs') ?>
    </head>
    <body>
        <nav class="navbar">
            <a href="/">Home</a>
            <a href="/index/add/">Add</a>
        </nav>
        <h1>Edit product</h1><?php if ($responseValue === 'editpicinvalid') { ?><div class="alert alert-danger">Invalid picture URL. Maybe it is empty?</div><?php } elseif ($responseValue === 'editfailed') { ?><div class="alert alert-danger">Product edit is failed. Contact to administrator.</div><?php } elseif ($responseValue === 'editinvalid') { ?><div class="alert alert-danger">Invalid name or price of product.</div><?php } elseif ($responseValue === 'invalidname') { ?><div class="alert alert-danger">Invalid name of product. It must be longer 4.</div><?php } elseif ($responseValue === 'invalidprice') { ?><div class="alert alert-danger">Invalid price of product.</div><?php } ?><form action="/index/editproduct" method="post">
            <input type="hidden" value="<?= $product_id ?>" name="product-id">
            <label for="product-name">Product name:</label>
            <input class="form-control" id="product-name" name="product-name" type="text" placeholder="Type a name of new product" value="<?= $product_name ?>">
            <br>
            <label for="product-coast">Product price:</label>
            <input class="form-control" id="product-price" name="product-price" type="text" placeholder="Type a price of new product" value="<?= $product_price ?>">
            <br>
            <label for="product-pictures-count">Product pictures count:</label>
            <div class="input-group">
                <input class="form-control" id="product-pictures-count" name="product-pictures-count" type="text" value="<?= $product_pic_count ?>" readonly>
                <div class="input-group-btn">
                    <button class="btn btn-default" id="product-pic-count-increase-btn" type="button">+</button>
                </div>
            </div>
            <br>
            <div id="product-count-urls-div">
                <?php $i = 0; ?>
                <?php foreach ($urls as $url) { ?>
                    <?php $i = $i + 1; ?>
                    <div id="product-url-<?= $i ?>" class="input-group">
                        <input type="hidden" name="product-id-<?= $i ?>" value="<?= $url['id'] ?>">
                        <input type="text" name="product-url-<?= $i ?>" class="form-control" value="<?= $url['url'] ?>">
                        <div class="input-group-btn">
                            <button name="product-remove-btn" type="button" class="btn btn-default" id="product-remove-btn-<?= $i ?>" onclick="removeOnClick($(this));">Remove</button>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <hr>
            <button class="btn btn-default" type="submit" name="product-create">Edit</button>
        </form>
    </body>
    <?= $this->assets->outputJs('footerJs') ?>
</html>