<html>
    <head>
        {{ assets.outputCss('headerCss') }}
        {{ assets.outputJs('headerJs') }}
    </head>
    <body>
        <nav class="navbar">
            <a href="/">Home</a>
            <a href="/index/add/">Add</a>
        </nav>
        <h1>Welcome</h1>

        {%- if responseValue === "createsuccess" -%}
        <div class="alert alert-success">Product has been created successfully.</div>
        {%- elseif responseValue === "deletesuccess" -%}
        <div class="alert alert-success">Product has been deleted successfully.</div>
        {%- elseif responseValue === "deletefailed" -%}
        <div class="alert alert-danger">Product deleting is failed. Contact to administrator.</div>
        {%- elseif responseValue === "invalidid" -%}
        <div class="alert alert-danger">Product with that ID does not exist.</div>
        {%- elseif responseValue === "editsuccess" -%}
        <div class="alert alert-success">Product has been edited successfully.</div>
        {%- endif -%}
        <table class="table">
            <thead>
            <tr>
                <td>Name</td>
                <td>Price</td>
                <td>Count pictures</td>
                <td></td>
                <td></td>
            </tr>
            </thead>
            <tbody>

            {%- if productsCount === 0 -%}
            <tr>
                <td colspan="5" style="text-align: center;"><span class="glyphicon glyphicon-info-sign"></span> No registered products.</td>
            </tr>
            {%- else -%}
            {% for product in products %}
                <tr>
                    <td>{{ product.name }}</td>
                    <td>{{ product.price }}</td>
                    <td>{{ product.pic_count }}</td>
                    <td><a class="btn btn-default" href="/index/remove/{{ product.id }}">Remove</a></td>
                    <td><a class="btn btn-default" href="/index/edit/{{ product.id }}">Edit</a></td>
                </tr>
            {% endfor %}
            {%- endif -%}
            </tbody>
        </table>
    </body>
</html>