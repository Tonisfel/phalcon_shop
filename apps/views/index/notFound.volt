<html>
    <head>
        {{ assets.outputCss('headerCss') }}
        {{ assets.outputJs('headerJs') }}
    </head>
    <body>
        <nav class="navbar">
            <a href="/">Home</a>
            <a href="/index/add/">Add</a>
        </nav>
        <h1>Not found :(</h1>

        <p>Page is not found!</p>
    </body>
</html>